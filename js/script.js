  function x2(n, i, x1, r) {
  	return x1 + r * Math.sin(2 * Math.PI * n / i);
  };

  function y2(n, i, y1, r) {
  	return y1 - r * Math.cos(2 * Math.PI * n / i);
  };

  $(function() {
  	var a;
  	var b;

  	function mostrar_hora() {
  		var d = new Date();
  		var dmes = d.getDay();
  		var dia = d.getDate();
  		var h = d.getHours();
  		var m = d.getMinutes();
  		var s = d.getSeconds();
  		var ds = d.getMilliseconds();
  		var segundos = s;
  		var minutos = m;
  		var horas = h;
  		var diam = {
  			0: "Dom ",
  			1: "Lun ",
  			2: "Mar ",
  			3: "Mier ",
  			4: "Jue ",
  			5: "Vier ",
  			6: "Sab "
  		};
  		var hoy = diam[dmes];

  		if (segundos < 10) {
  			segundos = "0" + segundos;
  		}
  		if (minutos < 10) {
  			minutos = "0" + minutos;
  		}
  		if (horas < 10) {
  			horas = "0" + horas;
  		}
  		if (dia < 10) {
  			dia = "0" + dia;
  		}

  		if ((h === a) && (m === b && s < 5)) {
  			$('#carrillon')[0].play();
  		}

  		$('#tex').html(horas + ":" + minutos + ":" + segundos);
  		$('#tex2').html(horas + ":" + minutos + ":" + segundos);
  		$('#seg').attr('x2', x2(s, 60, 100, 48)).attr('y2', y2(s, 60, 70, 48));
  		$('#min').attr('x2', x2(m, 60, 100, 40)).attr('y2', y2(m, 60, 70, 40));
  		$('#hor').attr('x2', x2(h, 12, 100, 30)).attr('y2', y2(h, 12, 70, 30));
  		$('#dscir').attr('x2', x2(ds, 1000, 100, 15)).attr('y2', y2(ds, 1000, 95, 15));
  		$('#tex3').html(hoy + dia);
  	}

  	function crear() {
  		a = $('#inh').val();
  		b = $('#inm').val();
  		a = +a;
  		b = +b;
  	}

  	$('#hecho').on('click', crear);
  	$('#hecho').on('tap', crear);

  	setInterval(function() {
  		mostrar_hora();
  	}, 10);
  	mostrar_hora();


  })