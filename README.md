# Reloj con svg
WebApp realizada como ejercicio opcional del módulo 9 de la 4ª edición del curso de la universidad politécnica de Madrid en Miriada x: Desarrollo en HTML5, CSS y Javascript de WebApps, incl. móviles FirefoxOS.  
Obtenga mas [infomación](https://myc-git.gitlab.io/mycweb/paginas/javascript/rsvg.html) o vealo en [funcionamiento](https://myc-git.gitlab.io/reloj_svg/).